package rejestracja.wyswietlanie;

import java.io.IOException;
import java.util.ArrayList;

import html.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import zarzadzanie.Impl.Userzy;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Userzy user = new Userzy("", "", "", "", "", "", "", "");

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String haslo = req.getParameter("haslo");
		String imie = req.getParameter("imie");
		String nazwisko = req.getParameter("nazwisko");
		String telefon = req.getParameter("telefon");
		String mail = req.getParameter("mail");
		String miasto = req.getParameter("miasto");
		String dataUr = req.getParameter("dataUr");

		Walidacja walidacja = new Walidacja();

		ArrayList<String> niepoprawne = new ArrayList<String>();
		if (walidacja.sprawdzPole("Login", login))
			this.user.login = login;
		else {
			niepoprawne.add("Login");
			this.user.login = "";
		}
		if (walidacja.sprawdzPole("Has�o", haslo))
			this.user.haslo = haslo;
		else {
			niepoprawne.add("Has�o");
			this.user.haslo = "";
		}
		if (walidacja.sprawdzPole("Imi�", imie))
			this.user.imie = imie;
		else {
			niepoprawne.add("Imi�");
			this.user.imie = "";
		}
		if (walidacja.sprawdzPole("Nazwisko", nazwisko))
			this.user.nazwisko = nazwisko;
		else {
			niepoprawne.add("Nazwisko");
			this.user.nazwisko = "";
		}
		if (walidacja.sprawdzPole("Telefon", telefon))
			this.user.telefon = telefon;
		else {
			niepoprawne.add("Telefon");
			this.user.telefon = "";
		}
		if (walidacja.sprawdzPole("Mail", mail))
			this.user.mail = mail;
		else {
			niepoprawne.add("Mail");
			this.user.mail = "";
		}
		if (walidacja.sprawdzPole("Miasto", miasto))
			this.user.miasto = miasto;
		else {
			niepoprawne.add("Miasto");
			this.user.miasto = "";
		}
		if (walidacja.sprawdzPole("Data urodzenia", dataUr))
			this.user.data_urodzenia = dataUr;
		else {
			niepoprawne.add("Data urodzenia");
			this.user.data_urodzenia = "";
		}

		if (!user.login.equals("") && !user.haslo.equals("")
				&& !user.imie.equals("") && !user.nazwisko.equals("")
				&& !user.telefon.equals("") && !user.mail.equals("")
				&& !user.miasto.equals("") && !user.data_urodzenia.equals("")
				&& user.sprawdz_login() != 1) {
			user.rejestracja();

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter()
					.write("<HTML><HEAD>"
							+ "<script type=\"text/javascript\">"
							+ "alert(\"Zosta�e� zarejestrowany poprawnie. Mo�esz si� zalogowa�!\");"
							+ "window.location=\"/zaloguj\"" + "</script>"
							+ "</HEAD></HTML>");

			System.out.println("Zarejestrowano u�ytkownika " + user.login);
		} else if (user.sprawdz_login() == 1) {
			System.out.println("Niepoprawna rejestracja, login istnieje");

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter()
					.write("<HTML><HEAD>"
							+ "<script type=\"text/javascript\">"
							+ "alert(\"Wybrany login jest ju� zaj�ty, rejestracja nieudana!\");"
							+ "window.location=\"/zarejestruj\"" + "</script>"
							+ "</HEAD></HTML>");
		} else {
			System.out.println("Niepoprawna rejestracja");
			String nPola = "";
			for (String pola : niepoprawne) {
				nPola += pola + "\\n";
			}
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(
					"<HTML><HEAD>" + "<script type=\"text/javascript\">"
							+ "alert(\"Niepoprawnie wype�nione pola:\\n"
							+ nPola + "\");"
							+ "window.location=\"/zarejestruj\"" + "</script>"
							+ "</HEAD></HTML>");

		}

		// user.rejestracja();
		// System.out.println("Zarejestrowano u�ytkownika" + login);

		// System.out.println("Niezarejestrowano u�ytkownika");
		// resp.sendRedirect("/zarejestruj?login="+login);

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1) {

			resp.getWriter().write(
					"<HTML><HEAD>" + "<script type=\"text/javascript\">"
							+ "alert(\"Wcze�niej si� wyloguj!\");"
							+ "window.location=\"/UserMojeZadania\""
							+ "</script>" + "</HEAD></HTML>");

		}

		String zawartosc = szablon
				.zamiana("<div id=\"form_container\">                                                                                                           "
						+ "	                                                                                                                                    "
						+ "		<h1><a<Rejestracja - Formularz</a></h1>                                                                                                   "
						+ "		<form id=\"form_953125\" class=\"appnitro\"  method=\"post\" action=\"\">                                                       "
						+ "					<div class=\"form_description\">                                                                                    "
						+ "		</div>						                                                                                                    "
						+ "			<ul >                                                                                                                       "
						+ "			                                                                                                                            "
						+ "					<li id=\"li_1\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"login\">Login                                                                            "
						+ "</label>                                                                                                                              "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"login\" name=\"login\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.login
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>6-45 znak�w, litery i cyfry</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_2\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"haslo\">Has�o </label>                                                                   "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"haslo\" name=\"haslo\" class=\"element text medium\" type=\"password\" maxlength=\"255\" value=\"\"/>       "
						+ "<span style=\"font-size:10px;\"><em>6-45 znak�w, litery i cyfry</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_3\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"imie\">Imi� </label>                                                                    "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"imie\" name=\"imie\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.imie
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>2-60 znak�w, litery i cyfry</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_4\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"nazwisko\">Nazwisko </label>                                                                "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"nazwisko\" name=\"nazwisko\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.nazwisko
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>2-60 znak�w, litery i cyfry</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_5\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"telefon\">Telefon </label>                                                                 "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"telefon\" name=\"telefon\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.telefon
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>9-cyfrowy numer telefonu</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_6\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"mail\">Mail </label>                                                                    "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"mail\" name=\"mail\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.mail
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>np. jan.kowalski@gmail.com</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_7\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"miasto\">Miasto </label>                                                                  "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"miasto\" name=\"miasto\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.miasto
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>Nazwa miasta</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>		<li id=\"li_8\" >                                                                                                   "
						+ "		<label class=\"description\" for=\"dataUr\">Data urodzenia </label>                                                          "
						+ "		<div>                                                                                                                           "
						+ "			<input id=\"dataUr\" name=\"dataUr\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\""
						+ this.user.data_urodzenia
						+ "\"/>       "
						+ "<span style=\"font-size:10px;\"><em>np.: 1990/01/25</em></span>"
						+ "		</div>                                                                                                                          "
						+ "		</li>                                                                                                                           "
						+ "			                                                                                                                            "
						+ "					<li class=\"buttons\">                                                                                              "
						+ "			    <input type=\"hidden\" name=\"form_id\" value=\"953125\" />                                                             "
						+ "			                                                                                                                            "
						+ "				<input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Submit\" />                        "
						+ "		</li>                                                                                                                           "
						+ "			</ul>                                                                                                                       "
						+ "		</form>	                                                                                                                        "
						+ "		<div id=\"footer\">                                                                                                             "
						+ "			                                                                   "
						+ "		</div>                                                                                                                          "
						+ "	</div>                                                                                                                              ");

		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(zawartosc);
	}
}