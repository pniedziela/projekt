package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.Userzy;

public class WszyscyUzytkownicy extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1 && login.equals("admin")) {
			String zawartosc = szablon
					.zamiana((String) polaczenie
							.wykonaj(
									"SELECT login,imie,nazwisko,telefon,mail,miasto,data_rejestracji,data_urodzenia FROM Userzy",
									"wykonaj-jtable"));

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		} else if (user.sprawdz_login_haslo() == 1) {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write("<HTML><HEAD>"
					+"<script type=\"text/javascript\">"
							+ "alert(\"Nie masz uprawnie� do odwiedzania tej strony!\");" 
					+ "window.location=\"/\""
					+ "</script>"
					+"</HEAD></HTML>");
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}