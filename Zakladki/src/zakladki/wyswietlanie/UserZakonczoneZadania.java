package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.Userzy;

public class UserZakonczoneZadania extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1) {

			
			
			String zawartosc = szablon
					.zamiana((String) polaczenie
							.wykonaj(
									"SELECT Zadania.id_zadania, Statusy_Zadan.data_statusu, Zadania.miasto, Zadania.opis_zadania, Statusy.nazwa_statusu      "
											+ "FROM Statusy_Zadan, Statusy, Zadania, Userzy                                                                            "
											+ "WHERE Userzy.login =  '"
											+ login
											+ "'                                                                                           "
											+ "AND Statusy.id_status =  '3'                                                                                            "
											+ "AND Zadania.id_zadania = Statusy_Zadan.Zadania_id_zadania                                                               "
											+ "AND Statusy_Zadan.Statusy_id_status = Statusy.id_status                                                                 "
											+ "AND Zadania.miasto = Userzy.miasto                                                                                      ",
									"wykonaj-jtable"));

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}