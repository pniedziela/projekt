package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.Userzy;

public class WszystkieZadania extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1 && login.equals("admin")) {
			String zawartosc = szablon
					.zamiana((String) polaczenie
							.wykonaj(
									"SELECT Zadania.id_zadania, Statusy_Zadan.data_statusu, Zadania.miasto, Zadania.telefon_zleceniodawcy, Zadania.mail_zleceniodawcy, Zadania.opis_zadania, Statusy_Zadan.data_statusu, Statusy.nazwa_statusu   "
											+ "FROM Zadania, Statusy_Zadan, Statusy                                                                                                                        "
											+ "WHERE Zadania.id_zadania = Statusy_Zadan.Zadania_id_zadania                                                                                                 "
											+ "AND Statusy_Zadan.Statusy_id_status = Statusy.id_status                                                                                                     ",
									"wykonaj-jtable-zad"));

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		}  else if (user.sprawdz_login_haslo() == 1) {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write("<HTML><HEAD>"
					+"<script type=\"text/javascript\">"
							+ "alert(\"Nie masz uprawnie� do odwiedzania tej strony!\");" 
					+ "window.location=\"/\""
					+ "</script>"
					+"</HEAD></HTML>");
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}