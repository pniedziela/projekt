package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import rejestracja.wyswietlanie.Walidacja;
import zarzadzanie.Impl.Userzy;

public class UserKonto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = (String) req.getSession().getAttribute("login");
		String haslo = req.getParameter("haslo");
		String telefon = req.getParameter("telefon");
		String mail = req.getParameter("mail");
		String miasto = req.getParameter("miasto");

		Walidacja waliduj = new Walidacja();

		PolaczenieImpl polaczenie = new PolaczenieImpl();

		if (haslo != null && waliduj.sprawdzPole("Has�o", haslo)) {
			polaczenie.wykonaj("UPDATE Userzy SET Userzy.haslo = '" + haslo
					+ "'" + " WHERE Userzy.login = '" + login + "';",
					"wykonaj-zapisz-int");
			System.out.println("Aktualizacja has�a");
		}
		
		if (telefon != null && waliduj.sprawdzPole("Telefon", telefon)) {
			polaczenie.wykonaj("UPDATE Userzy SET Userzy.telefon = '" + telefon
					+ "'" + " WHERE Userzy.login = '" + login + "';",
					"wykonaj-zapisz-int");
			System.out.println("Aktualizacja telefonu");
		}
		
		if (mail != null && waliduj.sprawdzPole("Mail", mail)) {
			polaczenie.wykonaj("UPDATE Userzy SET Userzy.mail = '" + mail
					+ "'" + " WHERE Userzy.login = '" + login + "'",
					"wykonaj-zapisz-int");
			System.out.println("Aktualizacja maila");
		}
		
		if (miasto != null && waliduj.sprawdzPole("Miasto", miasto)) {
			polaczenie.wykonaj("UPDATE Userzy SET Userzy.miasto = '" + miasto
					+ "'" + " WHERE Userzy.login = '" + login + "'",
					"wykonaj-zapisz-int");
			System.out.println("Aktualizacja miasta");
		}
		
		resp.sendRedirect("/UserKonto");
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1) {
			String zawartosc = szablon
					.zamiana("<b>Na tej stronie znajduj� si� informacje o Twoim koncie. Mo�esz je zmienia�.</b></br>"
							+ (String) polaczenie
									.wykonaj(
											"SELECT login,imie,nazwisko,telefon,mail,miasto,data_rejestracji,data_urodzenia FROM Userzy WHERE login='"
													+ login + "'",
											"wykonaj-jtable")
							+ "</br></br><table border=\"3\" style=\"background-color:#FFFFCC;border-collapse:collapse;border:3px solid #FFCC00;color:#000000;width:70%\" cellpadding=\"3\" cellspacing=\"3\">"
							+ "<tr><td>Has�o</td><td>Telefon</td><td>Mail</td><td>Miasto</td></tr>"
							+ "<form method=\"post\" action=\"\"> "
							+ "<td><input id=\"haslo\" name=\"haslo\" class=\"element text medium\" type=\"password\" maxlength=\"255\" value=\"\"/></td>"
							+ "<td><input id=\"telefon\" name=\"telefon\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/></td>"
							+ "<td><input id=\"mail\" name=\"mail\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/></td>"
							+ "<td><input id=\"miasto\" name=\"miasto\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/></td>"
							+ "<td><input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Zmie� dane\" /></td>"
							+ "</table>");

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}