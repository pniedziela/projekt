package stronaGlowna.wyswietlanie;

import java.io.IOException;

import html.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.osgi.service.http.*;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.AdministratorImpl;
import zarzadzanie.Impl.Userzy;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		String zawartosc = "";
		if (user.sprawdz_login_haslo() == 1) {

			resp.sendRedirect("/UserMojeZadania");

		} else {
			zawartosc = "<a href=\"/zaloguj\"><img src=\"http://www.mckvp.pl/zaloguj.jpg\"/></a></br><a href=\"/zarejestruj\"><img src=\"http://www.mckvp.pl/zarejestruj.png\"/></a>";
			resp.getWriter().write(szablon.zamiana(zawartosc));
		}

		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
	}
}