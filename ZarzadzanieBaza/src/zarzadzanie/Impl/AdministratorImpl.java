package zarzadzanie.Impl;


import zarzadzanie.Spec.*;
import polaczenieBaza.Impl.*;
import polaczenieBaza.spec.PolaczenieSpec;

public class AdministratorImpl implements AdministratorInt {

	PolaczenieSpec polaczenie = new PolaczenieImpl();

	@Override
	public String getUzytkownicy() {
		return (String) polaczenie
				.wykonaj(
						"SELECT login,imie,nazwisko,telefon,mail,miasto,data_rejestracji,data_urodzenia FROM Userzy",
						"wykonaj-jtable");
	}

	@Override
	public String getZadania() {
		// TODO Auto-generated method stub
		return (String) polaczenie
				.wykonaj(
						"SELECT Zadania.opis_zadania, Zadania.miasto, Zadania.mail_zleceniodawcy, Zadania.telefon_zleceniodawcy, Statusy_Zadan.data_statusu FROM Zadania,Statusy_Zadan WHERE Zadania.id_zadania=Statusy_Zadan.Zadania_id_zadania",
						"wykonaj-jtable");
	}
}
