package zarzadzanie.Impl;

import polaczenieBaza.Impl.PolaczenieImpl;
import polaczenieBaza.spec.PolaczenieSpec;

public class Userzy {
	// public int id_uzytkownika;
	public String login;
	public String haslo;
	public String imie;
	public String nazwisko;
	public String telefon;
	public String mail;
	public String miasto;
	public String poziom_zaufania;
	public String data_urodzenia;
	public String data_rejestracji;

	public Userzy(String login, String haslo, String imie, String nazwisko,
			String telefon, String mail, String miasto, String data_urodzenia) {
		this.login = login;
		this.haslo = haslo;
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.telefon = telefon;
		this.mail = mail;
		this.miasto = miasto;
		this.data_urodzenia = data_urodzenia;
	}

	public Userzy(String login, String haslo) {
		this.login = login;
		this.haslo = haslo;
	}

	public Userzy() {
		// TODO Auto-generated constructor stub
	}

	PolaczenieSpec polaczenie = new PolaczenieImpl();

	public int sprawdz_login_haslo() {

		return Integer.parseInt((String) polaczenie.wykonaj(
				"SELECT EXISTS( SELECT 1 FROM `plakaty`.`Userzy` WHERE `login` = '"
						+ login + "' AND `haslo` = '" + haslo + "')",
				"wykonaj-odczyt-int"));

		// return
		// "SELECT `login` ,`haslo` FROM `plakaty`.`Userzy` WHERE `login` = '"+login+"' AND `haslo` = '"+haslo+"'";

	}
	
	public int sprawdz_login() {

		return Integer.parseInt((String) polaczenie.wykonaj(
				"SELECT EXISTS( SELECT 1 FROM `plakaty`.`Userzy` WHERE `login` = '"
						+ login + "')",
				"wykonaj-odczyt-int"));

		// return
		// "SELECT `login` ,`haslo` FROM `plakaty`.`Userzy` WHERE `login` = '"+login+"' AND `haslo` = '"+haslo+"'";

	}

	public int rejestracja() {

		return (int) polaczenie
				.wykonaj(
						"INSERT INTO `plakaty`.`Userzy` (`id_uzytkownika` ,`login` ,`haslo` ,`imie` ,`nazwisko` ,`telefon` ,`mail` ,`miasto` ,`poziom_zaufania` ,`data_urodzenia` ,`data_rejestracji`)"
								+ "VALUES (NULL , '"
								+ login
								+ "' , '"
								+ haslo
								+ "' , '"
								+ imie
								+ "' , '"
								+ nazwisko
								+ "' , '"
								+ telefon
								+ "' , '"
								+ mail
								+ "' , '"
								+ miasto
								+ "' , '50', '"
								+ data_urodzenia
								+ "' , NOW( ));", "wykonaj-zapisz-int");

	}

}
