package html;

public class Szablony implements Zamieniarka {

	public static final String Poczatek = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"
			+ "<HTML>"
			+ "<HEAD >"
			+ "<style>                          "
			+ "#header {                        "
			+ "    background-color:black;      "
			+ "    color:white;                 "
			+ "    text-align:center;           "
			+ "    padding:5px;                 "
			+ "}                                "
			+ "#nav {                           "
			+ "    line-height:30px;            "
			+ "    background-color:#eeeeee "
			+ "    width:20%;                 "
			+ "    float:left;                  "
			+ "    padding:5px;                 "
			+ "    min-height:400px"
			+ "}                                "
			+ "#section {                       "
			+ "    width:70%;                 "
			+ "    float:left;                  "
			+ "    padding:10px;                "
			+ "    min-height:400px"
			+ "}                                "
			+ "#footer {                        "
			+ "    background-color:black;      "
			+ "    color:white;                 "
			+ "    clear:both;                  "
			+ "    text-align:center;           "
			+ "    padding:5px;                 "
			+ "}                                "
			+ "</style>                         "
			+ "<META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=iso-8859-2\">"
			+ "</HEAD>"
			+ "<BODY>"
			+ "<div id=\"header\">                                                                                "
			+ "<h1>System zarządzania pracami dorywczymi</h1>                                                                              "
			+ "</div>                                                                                             "
			+ "                                                                                                   "
			+ "<div id=\"nav\">    "
			+ "<a href=\"/zaloguj\">Zaloguj</a></br>"
			+ "<a href=\"/zarejestruj\">Zarejestruj</a></br>"
			+ "<a href=\"/DodajZlecenie\">Dodaj Zlecenie</a></br>"
			+ "<a href=\"/UserKonto\">Moje Konto</a></br>"
			+ "<a href=\"/UserWolneZadania\">Wolne Zadania</a></br>"
			+ "<a href=\"/UserMojeZadania\">Aktywne Zadania</a></br>"
			+ "<a href=\"/UserZakonczoneZadania\">Zakończone Zadania</a></br>"
			+ "<a href=\"/WszyscyUzytkownicy\">Użytkownicy</a></br>"
			+ "<a href=\"/WszystkieZadania\">Wszystkie Zadania</a></br>"
			+ "<a href=\"/zaloguj?wyloguj=\"logout\">Wyloguj</a></br>"
			+ "</div>                                                                                             "
			+ "                                                                                                   "
			+ "<div id=\"section\">                                                                               ";

	public static final String Poczatek2 = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">"
			+ "<HTML>"
			+ "<HEAD >"
			+ "<META HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=iso-8859-2\">"
			+ "</HEAD>" + "<BODY>";

	public static final String Koniec = "</div>                                                                                             "
			+ "                                                                                                   "
			+ "<div id=\"footer\">                                                                                "
			+ "Programowanie Komponentowe 2015 @ Przemysław Niedziela, Maciej Trznadel                                                                         "
			+ "</div>                                                                                             "
			+ "</BODY>" + "</HTML>";

	@Override
	public String zamiana(String napis) {
		return Poczatek + napis + Koniec;
	}
}
